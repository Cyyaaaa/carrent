﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZbW.CarRentify.CarManagement.Car_.Domain;
using ZbW.CarRentify.CarManagement.Car_.Services;
using ZbW.CarRentify.CarManagement.Category_.Api;

namespace ZbW.CarRentify.Tests
{
    public class CarServiceFake : ICarService
    {
        private readonly List<Car> _cars;
        public CarServiceFake()
        {
            _cars = new List<Car>()
                {
                   new Car(){Category = null, ConstructionYear = 2001, LastKm = 150001, Name = "Audi1", Id = new Guid("{3E407505-B014-42A9-B3FA-4E444BEEAB35}")},
                   new Car(){Category = null, ConstructionYear = 2002, LastKm = 150002, Name = "Audi2", Id = new Guid("{CD8E15AE-A5F9-4DD6-992A-75CB00DF80A2}")},
                   new Car(){Category = null, ConstructionYear = 2003, LastKm = 150003, Name = "Audi3", Id = new Guid("{FD04595B-63C3-437F-B98A-D721BFC27430}")},
                };
        }


        public void AddCar(Car car)
        {
            _cars.Add(car);
        }

        public void UpdateCar(Car newCar)
        {
            throw new NotImplementedException();
        }

        public void DeleteCar(Car carGuid)
        {
            throw new NotImplementedException();
        }

        public Car FindCar(Guid carGuid)
        {
            return _cars.FirstOrDefault(c => c.Id == carGuid);
        }

        public IEnumerable<Car> GetAllCars()
        {
            return _cars;
        }

        public IEnumerable<Car> GetCarsByCategory(CategoryDTO category)
        {
            throw new NotImplementedException();
        }

    }
}

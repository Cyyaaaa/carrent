﻿using Microsoft.AspNetCore.Mvc;
using System;
using Xunit;
using ZbW.CarRentify.CarManagement.Car_.Api;
using ZbW.CarRentify.CarManagement.Car_.Services;
using ZbW.CarRentify.CarManagement.Category_.Services;
//Test

namespace ZbW.CarRentify.Tests
{
    public class CarControllerTest
    {
        CarController _controller;
        ICarService _service;
        ICategoryService _categoryservice;


        public CarControllerTest()
        {
            _service = new CarServiceFake();
            _categoryservice = new CategoryServiceFake();
            _controller = new CarController(_service, _categoryservice);
        }

        [Fact]
        public void Get_CarById_ReturnsOk()
        {
            //Arrange
            //Act
            var okResult = _controller.GetCarById(new Guid("{FD04595B-63C3-437F-B98A-D721BFC27430}"));

            //Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void Get_CarById_ReturnsNotFoundResult()
        {
            //Arrange
            //Act
            var result = _controller.GetCarById(Guid.NewGuid());

            //Assert
            Assert.IsType<NotFoundObjectResult>(result);
        }

        [Fact]
        public void Get_GetAllCars_ReturnsOk()
        {
            //Arrange
            //Act
            var okResult = _controller.GetAllCars();
            //Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

    }
}

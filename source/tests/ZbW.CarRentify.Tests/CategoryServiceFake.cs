﻿using System;
using System.Collections.Generic;
using System.Text;
using ZbW.CarRentify.CarManagement.Category_.Domain;
using ZbW.CarRentify.CarManagement.Category_.Services;

namespace ZbW.CarRentify.Tests
{
    internal class CategoryServiceFake : ICategoryService
    {
        public void AddCategory(Category cat)
        {
            throw new NotImplementedException();
        }

        public void DeleteCategory(Category catGuid)
        {
            throw new NotImplementedException();
        }

        public Category FindCategory(Guid catGuid)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Category> GetAllCategories()
        {
            throw new NotImplementedException();
        }

        public void UpdateCategory(Category newCat)
        {
            throw new NotImplementedException();
        }
    }
}

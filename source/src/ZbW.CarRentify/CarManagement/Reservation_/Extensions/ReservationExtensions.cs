﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Car_.Services;
using ZbW.CarRentify.CarManagement.Reservation_.Api;
using ZbW.CarRentify.CarManagement.Reservation_.Domain;
using ZbW.CarRentify.CarManagement.User_.Services;

namespace ZbW.CarRentify.CarManagement.Reservation_.Extensions
{
    public static class ReservationExtensions
    {
        public static Reservation ToReservation(this ReservationDTO resdto, ICarService carService, IUserService userService)
        {
            var res = new Reservation();

            res.Id = resdto.Id;
            res.StartDate = resdto.StartDate;
            res.Days = resdto.Days;
            res.User = userService.FindUser(resdto.User);
            res.Car = carService.FindCar(resdto.Car);

            return res;
        }

        public static ReservationDTO ToReservationDTO(this Reservation res)
        {
            var resdto = new ReservationDTO();

            resdto.Id = res.Id;
            resdto.StartDate = res.StartDate;
            resdto.Days = res.Days;
            resdto.User = res.User.Id;
            resdto.Car = res.Car.Id;

            return resdto;
        }
    }
}
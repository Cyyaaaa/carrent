﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZbW.CarRentify.CarManagement.Reservation_.Api
{
    public class ReservationDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("car")]
        public Guid Car { get; set; }

        [JsonProperty("user")]
        public Guid User { get; set; }

        [JsonProperty("days")]
        public int Days { get; set; }

        [JsonProperty("startdate")]
        public DateTime StartDate { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ZbW.CarRentify.CarManagement.Car_.Services;
using ZbW.CarRentify.CarManagement.Category_.Services;
using ZbW.CarRentify.CarManagement.Reservation_.Extensions;
using ZbW.CarRentify.CarManagement.Reservation_.Services;
using ZbW.CarRentify.CarManagement.User_.Services;
using ZbW.CarRentify.Common;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ZbW.CarRentify.CarManagement.Reservation_.Api
{
    /// <summary>
    /// ReservationController is used to get and search for Reservations
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly ICarService _carService;
        private readonly IUserService _userService;
        private readonly IReservationService _reservationService;

        public ReservationController(ICategoryService categoryService, ICarService carService, IUserService userService, IReservationService reservationService)
        {
            _categoryService = categoryService;
            _carService = carService;
            _userService = userService;
            _reservationService = reservationService;
        }


        /// <summary>
        ///Get all Reservations
        /// </summary>
        [HttpGet("[action]")]
        public IActionResult GetAllReservations()
        {
            try
            {
                var ress = _reservationService.GetAllReservations();
                var returnRes = new List<ReservationDTO>();
                foreach (var res in ress)
                {
                    returnRes.Add(res.ToReservationDTO());
                }
                return Ok(returnRes);

            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }

        }
        

        /// <summary>
        /// Get a Reservations by User, Car or StartDate
        /// </summary>
        [HttpGet("[action]", Name = "GetReservations")]
        public IActionResult GetReservations(Guid userid, Guid carid, DateTime startDate)
        {
            try
            {
                var ress = _reservationService.GetReservations(userid, carid, startDate);
                var returnRes = new List<ReservationDTO>();
                foreach (var res in ress)
                {
                    returnRes.Add(res.ToReservationDTO());
                }
                return Ok(returnRes);
            }
            catch (EntityNotFoundException ex)
            {
                return base.NotFound(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Add a new Reservation from a ReservationDTO
        /// </summary>
        [HttpPost("[action]")]
        public IActionResult AddNewReservation([FromBody] ReservationDTO resdto)
        {
            try
            {
                var newRes = resdto.ToReservation(_carService, _userService);
                _reservationService.AddReservation(newRes);
                return Ok(newRes.ToReservationDTO());
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Deletes a Reservation by GUID
        /// </summary>
        [HttpDelete("[action]/{id}")]
        public IActionResult DeleteReservation(Guid id)
        {
            try
            {
                var deleteRes = _reservationService.FindReservation(id);
                _reservationService.DeleteReservation(deleteRes);
                return Ok();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }
    }
}

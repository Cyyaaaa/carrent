﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Car_.Api;
using ZbW.CarRentify.CarManagement.User_.Api;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.Reservation_.Domain
{
    public interface IReservationRepository : IRepository<Reservation>
    {
        IEnumerable<Reservation> GetReservations(Guid userid, Guid carid, DateTime datetime);
    }
}

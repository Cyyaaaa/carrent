﻿using System;
using ZbW.CarRentify.CarManagement.Car_.Domain;
using ZbW.CarRentify.CarManagement.User_.Domain;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.Reservation_.Domain
{
    public class Reservation : EntityBase
    {
        public Car Car { get; set; }
        public User User { get; set; }
        public int Days { get; set; }
        public DateTime StartDate { get; set; }

    }
}

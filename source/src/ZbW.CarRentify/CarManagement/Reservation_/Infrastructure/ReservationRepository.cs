﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using ZbW.CarRentify.CarManagement.Car_.Api;
using ZbW.CarRentify.CarManagement.Car_.Extensions;
using ZbW.CarRentify.CarManagement.Infrastructure;
using ZbW.CarRentify.CarManagement.Reservation_.Domain;
using ZbW.CarRentify.CarManagement.User_.Api;
using ZbW.CarRentify.CarManagement.User_.Extensions;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.Reservation_.Infrastructure
{
    public class ReservationRepository : IReservationRepository
    {
        private readonly CarContext _carContext;
        public ReservationRepository(CarContext carContext)
        {
            _carContext = carContext;
        }


        public void Delete(Reservation entity)
        {
            _carContext.Reservations.Remove(entity);
            _carContext.SaveChanges();
        }

        public Reservation Get(Guid id)
        {
            var reservation = _carContext.Reservations.Find(id);
            
            if(reservation == null)
            {
                throw new EntityNotFoundException();
            }
            return reservation;
        }

        public IEnumerable<Reservation> GetAll()
        {
            return _carContext.Reservations.Include(r => r.Car).Include(r => r.User);
        }

        public IEnumerable<Reservation> GetReservations(Guid userid, Guid carid, DateTime datetime)
        {
            //Todo: Add from - to Date
            var reservations = _carContext.Reservations.Include(r => r.Car).Include(r => r.User).ToList();

            if(userid != null && userid != Guid.Empty)
            {
                reservations = reservations.Where(r => r.User.Id == userid).ToList();
            }
            if(carid != null && carid != Guid.Empty)
            {
                reservations = reservations.Where(r => r.Car.Id == carid).ToList();
            }
            if(datetime != null && datetime != default(DateTime))
            {
                reservations = reservations.Where(r => r.StartDate.Date == datetime.Date).ToList();
            }

            return reservations;
        }

        public void Insert(Reservation entity)
        {
            _carContext.Reservations.Add(entity);
            _carContext.SaveChanges();
        }

        public void Update(Reservation entity)
        {
            
        }
    }
}

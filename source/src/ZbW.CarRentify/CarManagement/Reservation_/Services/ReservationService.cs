﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Car_.Api;
using ZbW.CarRentify.CarManagement.Reservation_.Domain;
using ZbW.CarRentify.CarManagement.User_.Api;

namespace ZbW.CarRentify.CarManagement.Reservation_.Services
{
    public class ReservationService : IReservationService
    {
        private readonly ILogger<ReservationService> _logger;
        private readonly IReservationRepository _resRepository;


        public ReservationService(IReservationRepository resRepository, ILogger<ReservationService> logger)
        {
            _logger = logger;
            _resRepository = resRepository;
        }


        public void AddReservation(Reservation reservation)
        {
            _resRepository.Insert(reservation);
        }

        public void DeleteReservation(Reservation reservation)
        {
            _resRepository.Delete(reservation);
        }

        public Reservation FindReservation(Guid reservationGuid)
        {
            return _resRepository.Get(reservationGuid);
        }

        public IEnumerable<Reservation> GetAllReservations()
        {
            return _resRepository.GetAll();
        }

        public IEnumerable<Reservation> GetReservations(Guid userid, Guid carid, DateTime datetime)
        {
            return _resRepository.GetReservations(userid, carid, datetime);
        }

    }
}

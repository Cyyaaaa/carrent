﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Car_.Api;
using ZbW.CarRentify.CarManagement.Reservation_.Domain;
using ZbW.CarRentify.CarManagement.User_.Api;

namespace ZbW.CarRentify.CarManagement.Reservation_.Services
{
    public interface IReservationService
    {
        void AddReservation(Reservation reservation);
        void DeleteReservation(Reservation reservation);      
        Reservation FindReservation(Guid reservationGuid);
        IEnumerable<Reservation> GetAllReservations();

        IEnumerable<Reservation> GetReservations(Guid userid, Guid carid, DateTime datetime);
        
        //TODO: Add Cancle reservation (delete)
    }
}

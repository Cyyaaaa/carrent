﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Category_.Domain;
using ZbW.CarRentify.CarManagement.Infrastructure;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.Category_.Infrastructure
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly CarContext _carContext;

        public CategoryRepository(CarContext carContext)
        {
            _carContext = carContext;
        }


        public void Delete(Category entity)
        {
            _carContext.Categories.Remove(entity);
            _carContext.SaveChanges();
        }

        public Category Get(Guid id)
        {
            var cat = _carContext.Categories.Find(id);
            if (cat == null)
            {
                throw new EntityNotFoundException();
            }

            return cat;
        }

        public IEnumerable<Category> GetAll()
        {
            return _carContext.Categories;
        }

        public void Insert(Category entity)
        {
            _carContext.Categories.Add(entity);
            _carContext.SaveChanges();
        }

        public void Update(Category entity)
        {
            _carContext.Categories.Update(entity);
            _carContext.SaveChanges();
        }
    }
}

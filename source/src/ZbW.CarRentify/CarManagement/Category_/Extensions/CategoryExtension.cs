﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Category_.Api;
using ZbW.CarRentify.CarManagement.Category_.Domain;

namespace ZbW.CarRentify.CarManagement.Category_.Extensions
{
    public static class CategoryExtension
    {
        /// <summary>
        /// Extension to convert a CategoryDTO to a Category
        /// </summary>
        public static Category ToCategory(this CategoryDTO categorydto)
        {

            //check for null values
            var category = new Category();
            category.Id = categorydto.Id;
            category.Name = categorydto.Name;
            category.DailyFee = categorydto.DailyFee;

            return category;
        }

        /// <summary>
        /// Extension to convert a Category to a CategoryDTO
        /// </summary>
        public static CategoryDTO ToCategoryDTO(this Category category)
        {
            var categorydto = new CategoryDTO();

            categorydto.Id = category.Id;

            if (category.Name != null)
            {
                categorydto.Name = category.Name;
            }

            categorydto.DailyFee = category.DailyFee;

            return categorydto;
        }

    }
}

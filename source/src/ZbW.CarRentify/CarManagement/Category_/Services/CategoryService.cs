﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Category_.Domain;

namespace ZbW.CarRentify.CarManagement.Category_.Services
{
    public class CategoryService : ICategoryService
    {

        private readonly ILogger<CategoryService> _logger;
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository, ILogger<CategoryService> logger)
        {
            _categoryRepository = categoryRepository;
            _logger = logger;
        }


        public void AddCategory(Category cat)
        {
            _categoryRepository.Insert(cat);
        }

        public void DeleteCategory(Category catGuid)
        {
            _categoryRepository.Delete(catGuid);
        }

        public Category FindCategory(Guid catGuid)
        {
            return _categoryRepository.Get(catGuid);
        }

        public IEnumerable<Category> GetAllCategories()
        {
            return _categoryRepository.GetAll();
        }

        public void UpdateCategory(Category newCat)
        {
            _categoryRepository.Update(newCat);
        }
    }
}

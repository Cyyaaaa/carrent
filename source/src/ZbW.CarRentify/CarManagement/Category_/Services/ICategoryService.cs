﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Category_.Domain;

namespace ZbW.CarRentify.CarManagement.Category_.Services
{
    public interface ICategoryService
    {
        void AddCategory(Category cat);
        void UpdateCategory(Category newCat);
        void DeleteCategory(Category catGuid);
        Category FindCategory(Guid catGuid);
        IEnumerable<Category> GetAllCategories();

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.Category_.Domain
{
    public class Category : EntityBase
    {
        
        public string Name { get; set; }
        public decimal DailyFee { get; set; }

    }
}

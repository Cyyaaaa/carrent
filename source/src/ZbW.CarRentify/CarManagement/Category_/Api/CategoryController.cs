﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ZbW.CarRentify.CarManagement.Category_.Extensions;
using ZbW.CarRentify.CarManagement.Category_.Services;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.Category_.Api
{
    /// <summary>
    /// CategoryController is used to get, update and delete Categories
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        /// <summary>
        ///Get all Categories
        /// </summary>
        [HttpGet("[action]")]
        public IActionResult GetAllCategories()
        {
            try
            {
                var cats = _categoryService.GetAllCategories();
                var returnCatDtos = new List<CategoryDTO>();
                foreach (var cat in cats)
                {
                    returnCatDtos.Add(cat.ToCategoryDTO());
                }
                return Ok(returnCatDtos);

            } //TODO: find a better exception and statuscode
            catch (Exception ex)
            {
                return NotFound(ex);
            }

        }


        /// <summary>
        /// Get a specific Category by GUID
        /// </summary>
        [HttpGet("[action]/{id}", Name = "GetCategoryByID")]
        public IActionResult GetCategoryByID(Guid id)
        {
            try
            {
                var cat = _categoryService.FindCategory(id);
                return Ok(cat.ToCategoryDTO()); //200 rückgabe
            }
            catch (EntityNotFoundException ex)
            {
                //hier noch infos loggen
                return base.NotFound(ex); //4xx
                //return base.StatusCode(666);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Add a new Category from a CategoryDTO
        /// </summary>
        [HttpPost("[action]")]
        public IActionResult AddNewCategory([FromBody] CategoryDTO catdto)
        {
            try
            {
                var newCat = catdto.ToCategory();
                _categoryService.AddCategory(newCat);
                return Ok(catdto);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }


        }

        /// <summary>
        /// Updates a Category
        /// </summary>
        [HttpPut("[action]/{id}")]
        public IActionResult UpdateCategory(Guid id, [FromBody] CategoryDTO catdto)
        {
            try
            {
                var cat = _categoryService.FindCategory(id);
                cat.Name = catdto.Name;
                cat.DailyFee = catdto.DailyFee;

                _categoryService.UpdateCategory(cat);
                return Ok(cat.ToCategoryDTO());
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Deletes a Category by GUID
        /// </summary>
        [HttpDelete("[action]/{id}")]
        public IActionResult DeleteCategory(Guid id)
        {
            try
            {
                var deleteCat = _categoryService.FindCategory(id);
                _categoryService.DeleteCategory(deleteCat);
                return Ok();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }
    }
}

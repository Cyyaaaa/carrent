﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Car_.Api;
using ZbW.CarRentify.CarManagement.Car_.Domain;
using ZbW.CarRentify.CarManagement.Category_.Extensions;
using ZbW.CarRentify.CarManagement.Category_.Services;
using ZbW.CarRentify.CarManagement.Infrastructure;

namespace ZbW.CarRentify.CarManagement.Car_.Extensions
{
    public static class CarExtension
    {
        /// <summary>
        /// Extension to convert a CarDTO to a Car 
        /// </summary>
        public static Car ToCar(this CarDTO cardto, ICategoryService categoryService)
        {
            var car = new Car();
            car.Id = cardto.Id;
            car.Name = cardto.Name;
            car.LastKm = int.Parse(cardto.LastKm);
            car.ConstructionYear = int.Parse(cardto.ConstructionYear);
            //TODO: Test this
            car.Category = categoryService.FindCategory(cardto.Category.Id);
            return car;
        }

        /// <summary>
        /// Extension to convert a Car to a CarDTO
        /// </summary>
        public static CarDTO ToCarDTO(this Car car)
        {
            var cardto = new CarDTO();

            cardto.Id = car.Id;

            if (car.Name != null)
            {
                cardto.Name = car.Name;
            }

            if (car.LastKm != null)
            {
                cardto.LastKm = car.LastKm.ToString();
            }

            if (car.ConstructionYear != null)
            {
                cardto.ConstructionYear = car.ConstructionYear.ToString();
            }

            if (car.Category != null)
            {
                cardto.Category = car.Category.ToCategoryDTO();
            }

            return cardto;
        }

    }
}

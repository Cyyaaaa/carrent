﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Car_.Domain;
using ZbW.CarRentify.CarManagement.Category_.Api;
using ZbW.CarRentify.CarManagement.Category_.Extensions;
using ZbW.CarRentify.CarManagement.Infrastructure;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.Car_.Infrastructure
{
    public class CarRepository : ICarRepository
    {
        private readonly CarContext _carContext;

        public CarRepository(CarContext carContext)
        {
            _carContext = carContext;
        }

        public void Delete(Car entity)
        {
            _carContext.Cars.Remove(entity);
            _carContext.SaveChanges();
        }

        public Car Get(Guid id)
        {
            var car = _carContext.Cars.Find(id);
            if (car == null)
            {
                throw new EntityNotFoundException();
            }

            return car;
        }

        /// <summary>
        /// Returns all Cars
        /// </summary>
        public IEnumerable<Car> GetAll()
        {
            var cars = _carContext.Cars.Include(c => c.Category);
            return cars;
        }

        public IEnumerable<Car> GetCarsByCategory(CategoryDTO category)
        {
            return _carContext.Cars.Include(c => c.Category).Where(c => c.Category.Id == category.ToCategory().Id);
        }

        public void Insert(Car entity)
        {
            _carContext.Cars.Add(entity);
            _carContext.SaveChanges();
        }

        public void Update(Car entity)
        {
            _carContext.Update(entity);
            _carContext.SaveChanges();
        }
    }
}

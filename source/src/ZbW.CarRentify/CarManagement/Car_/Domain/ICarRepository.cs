﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Category_.Api;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.Car_.Domain
{
    public interface ICarRepository : IRepository<Car>
    {
        IEnumerable<Car> GetCarsByCategory(CategoryDTO category);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Category_.Domain;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.Car_.Domain
{
    public class Car : EntityBase
    {
        public Category Category { get; set; }
        public int LastKm { get; set; }
        public int ConstructionYear { get; set; }
        public string Name { get; set; }

    }
}

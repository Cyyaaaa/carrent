﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ZbW.CarRentify.CarManagement.Car_.Extensions;
using ZbW.CarRentify.CarManagement.Car_.Services;
using ZbW.CarRentify.CarManagement.Category_.Api;
using ZbW.CarRentify.CarManagement.Category_.Services;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.Car_.Api
{
    /// <summary>
    /// Carcontroller is used to create, midify and delete cars
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CarController : ControllerBase
    {
        private readonly ICarService _carService;
        private readonly ICategoryService _categoryService;

        public CarController(ICarService carService, ICategoryService categoryService)
        {
            _carService = carService;
            _categoryService = categoryService;
        }

        //TODO: Fix all errorhanldings

        /// <summary>
        /// Return all cars
        /// </summary>
        [HttpGet("[action]")]
        public IActionResult GetAllCars()
        {
            try
            {
                var cars = _carService.GetAllCars();
                var returnCarDtos = new List<CarDTO>();
                foreach (var car in cars)
                {
                    returnCarDtos.Add(car.ToCarDTO());
                }

                return Ok(returnCarDtos);
            } 
            catch (Exception ex)
            {
                return NotFound(ex);
            }

        }

        /// <summary>
        /// Get a specific Car by GUID
        /// </summary>
        [HttpGet("[action]/{id}", Name = "GetCarById")]
        public IActionResult GetCarById(Guid id)
        {
            try
            {
                var car = _carService.FindCar(id);
                return Ok(car.ToCarDTO()); //200 rückgabe
            }
            catch (EntityNotFoundException ex)
            {
                //hier noch infos loggen
                return base.NotFound(ex); //4xx
                //return base.StatusCode(666);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Get a specific Car by CategoryDTO
        /// </summary>
        [HttpPost("[action]")]
        public IActionResult GetCarByCategory([FromBody] CategoryDTO category)
        {
            try
            {
                var cars = _carService.GetCarsByCategory(category);
                var returnCarDtos = new List<CarDTO>();
                foreach (var car in cars)
                {
                    returnCarDtos.Add(car.ToCarDTO());
                }

                return Ok(returnCarDtos); 
            }
            catch (EntityNotFoundException ex)
            {
                return base.NotFound(ex);
            }
            catch (NullReferenceException ex)
            {
                return NotFound("No Car with this Category found - " + ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }


        /// <summary>
        /// Add a new Car from a CarDTO
        /// </summary>
        [HttpPost("[action]")]
        public IActionResult AddNewCar([FromBody] CarDTO cardto)
        {
            try
            {
                var newCar = cardto.ToCar(_categoryService);
                _carService.AddCar(newCar);
                return Ok(cardto);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }


        }

        /// <summary>
        /// modify a Car
        /// </summary>
        [HttpPut("[action]/{id}")]
        public IActionResult UpdateCar(Guid id, [FromBody] CarDTO cardto)
        {
            try
            {
                var car = _carService.FindCar(id);
                car.LastKm = int.Parse(cardto.LastKm);
                car.Name = cardto.Name;
                car.ConstructionYear = int.Parse(cardto.ConstructionYear);

                _carService.UpdateCar(car);
                return Ok(cardto);
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }

        }

        /// <summary>
        /// Deletes a Car per guid
        /// </summary>
        [HttpDelete("[action]/{id}")]
        public IActionResult DeleteCar(Guid id)
        {
            try
            {
                var deleteCar = _carService.FindCar(id);
                _carService.DeleteCar(deleteCar);
                return Ok();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }

        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Category_.Api;

namespace ZbW.CarRentify.CarManagement.Car_.Api
{
    public class CarDTO
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("lastkm")]
        public string LastKm { get; set; }

        [JsonProperty("category")]
        public CategoryDTO Category { get; set; }

        [JsonProperty("constructionyear")]
        public string ConstructionYear { get; set; }

    }
}

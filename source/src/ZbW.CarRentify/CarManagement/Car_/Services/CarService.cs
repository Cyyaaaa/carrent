﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Car_.Domain;
using ZbW.CarRentify.CarManagement.Category_.Api;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.Car_.Services
{
    public class CarService : ICarService
    {
        private readonly ILogger<CarService> _logger;
        private readonly ICarRepository _carRepository;


        public CarService(ICarRepository carRepository, ILogger<CarService> logger)
        {
            _logger = logger;
            _carRepository = carRepository;
        }

        public void AddCar(Car car)
        {
            _carRepository.Insert(car);
        }

        public void UpdateCar(Car newCar)
        {
            _carRepository.Update(newCar);
        }

        public void DeleteCar(Car carGuid)
        {
            _carRepository.Delete(carGuid);
        }

        public Car FindCar(Guid carGuid)
        {
            return _carRepository.Get(carGuid);
        }

        public IEnumerable<Car> GetAllCars()
        {
            return _carRepository.GetAll();
        }

        public IEnumerable<Car> GetCarsByCategory(CategoryDTO category)
        {
            return _carRepository.GetCarsByCategory(category);
        }
    }

}

﻿using System;
using System.Collections.Generic;
using ZbW.CarRentify.CarManagement.Car_.Domain;
using ZbW.CarRentify.CarManagement.Category_.Api;

namespace ZbW.CarRentify.CarManagement.Car_.Services
{
    public interface ICarService
    {
        void AddCar(Car car);
        void UpdateCar(Car newCar);
        void DeleteCar(Car carGuid);
        Car FindCar(Guid carGuid);
        IEnumerable<Car> GetAllCars();

        IEnumerable<Car> GetCarsByCategory(CategoryDTO category);
    }
}

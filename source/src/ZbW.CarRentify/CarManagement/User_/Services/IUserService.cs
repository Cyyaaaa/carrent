﻿using System;
using System.Collections.Generic;
using ZbW.CarRentify.CarManagement.User_.Domain;

namespace ZbW.CarRentify.CarManagement.User_.Services
{
    public interface IUserService
    {
        void AddUser(User user);
        void UpdateUser(User user);
        User FindUser(Guid userGuid);
        IEnumerable<User> GetAllUsers();
        User FindUser(string name);
    }
}

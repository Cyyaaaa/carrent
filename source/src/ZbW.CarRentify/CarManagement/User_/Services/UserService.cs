﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.User_.Domain;

namespace ZbW.CarRentify.CarManagement.User_.Services
{
    public class UserService : IUserService
    {

        private readonly ILogger<UserService> _logger;
        private readonly IUserRepository _userRepository;


        public UserService(IUserRepository userRepository, ILogger<UserService> logger)
        {
            _logger = logger;
            _userRepository = userRepository;
        }


        public void AddUser(User user)
        {
            _userRepository.Insert(user);
        }

        public User FindUser(Guid userGuid)
        {
            return _userRepository.Get(userGuid);
        }

        public User FindUser(string name)
        {
            return _userRepository.GetUserByName(name);
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _userRepository.GetAll();
        }

        public void UpdateUser(User user)
        {
            _userRepository.Update(user);
        }
    }
}

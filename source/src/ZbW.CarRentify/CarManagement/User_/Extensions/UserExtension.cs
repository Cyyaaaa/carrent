﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.User_.Api;
using ZbW.CarRentify.CarManagement.User_.Domain;

namespace ZbW.CarRentify.CarManagement.User_.Extensions
{
    public static class UserExtension
    {

        /// <summary>
        /// Extension to convert a UserDTO to a User
        /// </summary>returns>
        public static User ToUser(this UserDTO userdto)
        {
            var user = new User();
            user.Id = userdto.Id;
            user.IsIntern = userdto.IsIntern;
            user.Name = userdto.Name;
            user.Zip = userdto.Zip;
            user.Address = userdto.Address;

            return user;
        }



        /// <summary>
        /// Extension to convert a User to a UserDTO
        /// </summary>
        public static UserDTO ToUserDTO(this User user)
        {
            var userdto = new UserDTO();

            userdto.Id = user.Id;
            if (user.IsIntern != null)
            {
                userdto.IsIntern = user.IsIntern;
            }

            if (user.Name != null)
            {
                userdto.Name = user.Name;
            }
            if (user.Zip != null)
            {
                userdto.Zip = user.Zip;
            }
            if (user.Address != null)
            {
                userdto.Address = user.Address;
            }

            return userdto;
        }
    }
}

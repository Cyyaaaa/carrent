﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.CarManagement.Infrastructure;
using ZbW.CarRentify.CarManagement.User_.Domain;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.User_.Infrastructure
{
    public class UserRepository : IUserRepository
    {
        private readonly CarContext _carContext;

        public UserRepository(CarContext carContext)
        {
            _carContext = carContext;
        }


        public void Delete(User entity)
        {
            _carContext.Users.Remove(entity);
            _carContext.SaveChanges();
        }

        public User Get(Guid id)
        {
            var user = _carContext.Users.Find(id);
            if (user == null)
            {
                throw new EntityNotFoundException();
            }

            return user;
        }

        public IEnumerable<User> GetAll()
        {
            return _carContext.Users;
        }

        public User GetUserByName(string name)
        {
            var user = _carContext.Users.Where(u => u.Name == name).First();
            if(user == null)
            {
                throw new EntityNotFoundException();
            }
            return user;
        }

        public void Insert(User entity)
        {
            _carContext.Users.Add(entity);
            _carContext.SaveChanges();
        }

        public void Update(User entity)
        {
            _carContext.Users.Update(entity);
            _carContext.SaveChanges();
        }
    }
}

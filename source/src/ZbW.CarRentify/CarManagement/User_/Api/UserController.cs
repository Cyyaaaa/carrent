﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ZbW.CarRentify.CarManagement.User_.Extensions;
using ZbW.CarRentify.CarManagement.User_.Services;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.User_.Api
{
    /// <summary>
    /// Usercontroller is used to get, update and delete Users
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        ///Get all Users
        /// </summary>
        [HttpGet("[action]")]
        public IActionResult GetAllUsers()
        {
            try
            {
                var users = _userService.GetAllUsers();
                var returnUserDtos = new List<UserDTO>();
                foreach (var user in users)
                {
                    returnUserDtos.Add(user.ToUserDTO());
                }
                return Ok(returnUserDtos);

            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }

        }



        //--------------------------------------
        /// <summary>
        /// Get a specific User by GUID
        /// </summary>
        [HttpGet("[action]/{id}", Name = "GetUserByID")]
        public IActionResult GetUserByID(Guid id)
        {
            try
            {
                var user = _userService.FindUser(id);
                return Ok(user.ToUserDTO());
            }
            catch (EntityNotFoundException ex)
            {
                return base.NotFound(ex); 
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Get a specific User by Name
        /// </summary>
        [HttpGet("[action]/{name}", Name = "GetUserByName")]
        public IActionResult GetUserByName(string name)
        {
            try
            {
                var user = _userService.FindUser(name);
                return Ok(user.ToUserDTO());
            }
            catch (EntityNotFoundException ex)
            {
                return base.NotFound(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        /// <summary>
        /// Add a new User from a UserDTO
        /// </summary>
        [HttpPost("[action]")]
        public IActionResult AddNewUser([FromBody] UserDTO userdto)
        {
            try
            {
                var newUser = userdto.ToUser();
                _userService.AddUser(newUser);
                return Ok(newUser.ToUserDTO());
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }


        }

        /// <summary>
        /// Updates a User
        /// </summary>
        [HttpPut("[action]/{id}")]
        public IActionResult UpdateUser(Guid id, [FromBody] UserDTO userdto)
        {
            try
            {
                var user = _userService.FindUser(id);
                user.Name = userdto.Name;
                user.Zip = userdto.Zip;
                user.Address = userdto.Address;

                _userService.UpdateUser(user);
                return Ok(user.ToUserDTO());
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(ex);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.User_.Domain
{
    public interface IUserRepository : IRepository<User>
    {
        User GetUserByName(string name);
    }
}

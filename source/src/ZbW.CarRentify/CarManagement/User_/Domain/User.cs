﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZbW.CarRentify.Common;

namespace ZbW.CarRentify.CarManagement.User_.Domain
{
    public class User : EntityBase
    {

        public string Name { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public bool IsIntern { get; set; }


    }
}

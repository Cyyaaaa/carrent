﻿using Microsoft.EntityFrameworkCore;
using ZbW.CarRentify.CarManagement.Car_.Domain;
using ZbW.CarRentify.CarManagement.Category_.Domain;
using ZbW.CarRentify.CarManagement.Reservation_.Domain;
using ZbW.CarRentify.CarManagement.User_.Domain;

namespace ZbW.CarRentify.CarManagement.Infrastructure
{
    public class CarContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<User> Users { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=carrent;Trusted_Connection=True;");
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=carrent;Trusted_Connection=True;");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var carEntity = modelBuilder.Entity<Car>();
            carEntity.HasKey(x => x.Id);
            carEntity.HasOne(x => x.Category);
            carEntity.UsePropertyAccessMode(PropertyAccessMode.Field);

            var categoryEntity = modelBuilder.Entity<Category>();
            categoryEntity.HasKey(x => x.Id);
            categoryEntity.UsePropertyAccessMode(PropertyAccessMode.Field);

            var reservationEntity = modelBuilder.Entity<Reservation>();
            reservationEntity.HasKey(x => x.Id);
            reservationEntity.HasOne(x => x.Car);
            reservationEntity.HasOne(x => x.User);
            reservationEntity.UsePropertyAccessMode(PropertyAccessMode.Field);

            var userEntity = modelBuilder.Entity<User>();
            userEntity.HasKey(x => x.Id);
            userEntity.UsePropertyAccessMode(PropertyAccessMode.Field);
            
            base.OnModelCreating(modelBuilder);
        }
    }
}

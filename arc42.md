**Über arc42**

arc42, das Template zur Dokumentation von Software- und
Systemarchitekturen.

Erstellt von Dr. Gernot Starke, Dr. Peter Hruschka und Mitwirkenden.

Template Revision: 7.0 DE (asciidoc-based), January 2017

© We acknowledge that this document uses material from the arc42
architecture template, <http://www.arc42.de>. Created by Dr. Peter
Hruschka & Dr. Gernot Starke.


Einführung und Ziele
====================

Die Firma "CarRent" will ein neues Autovermietungssystem.
Das System soll aus einem Server-Teil und zu einem späteren Zeitpunkt, einem Web-Client bestehen.

Dieses Projekt befasst sich nur mit dem Server-Teil.


Aufgabenstellung
----------------

**Inhalt.**

a) Die Daten sollen mittels Repository Pattern in eine Datenbank gespeichert werden können.
b) Die Business Logik soll auf dem Backend laufen und REST APIs anbieten.
c) Es soll zuerst ein Monolith erstellt werden und später auf eine Micro Service Architektur überführt
werden.


Randbedingungen
===============

**Inhalt.**

Das Backend soll eine REST API anbieten und auf einem eigenen Server laufen.
Für die Datenbank sind keine genauen Anforderungen bekannt.


Kontextabgrenzung 
=================

**Inhalt.**

Das System soll komplett selbstständig auf einem eigenen Server laufen. 
Es gibt noch keine vorhandene Datenbank die in das neue System mit einbezogen werden muss.


Fachlicher Kontext
------------------

**Inhalt.**

Die Architektur des System besteht aus einer Datenbank und einer API. Die API und die Datenbank werden sich auf dem selben Server befinden.
Zu einem späteren Zeitpunkt soll es möglich sein, dass mit Hilfe einer Website/Web-Applikation auf die API zugegriffen werden kann.


Technischer Kontext 
-------------------

**Inhalt.**

Es steht eine MS SQL Datenbank zur verfügung.
Diese wird von einer ASP.NET Web API angesprocehen

Die API wird von Kunden sowie von internen angestellten der Firma "CarRent" benutzt.
Dies geschiet in Zukunft über eine Web-Oberfläche, welche noch nicht definiert wurde.


Lösungsstrategie 
================

**Inhalt.**

Technologie: 
Die Datenbank wird auf einem MS SQL Server gespeichert.

Hier folgen wir den Anforderungen der Firma und setzen eine ASP.NET API ein. 


Bausteinsicht 
=============

**Inhalt.**

Das System wir in folgende Container aufgeteilt:
- Logik
- Bereitsteller der Funktion
- Benutzer

In diesem Projekt wurde der Logik Teil des Systems umgesetzt.

Zu unterst ist das Domänenmodel. Es besteht aus mehreren Entitäten:
- Klasse    	Die Klasse des Autos. Darin befindet sich z.B. der Preis pro Tag
- Auto      	Das Autoobjekt beinhaltet die Beschreibung des Fahrzeugs sowie die gefahrenen Kilometer
- Reservation   Der Kern des Reservationssystems. Die Entität beinhaltet alle Reservationen die bereits gemacht wurden. Sie verküpft den Kunden mit einem Auto
- Kunde     	Ein Kunde kann ein Auto reservieren. Er besteht aus einem User und einer Adresse.
- Adresse  	 	Die verschiedenen Adressen der Kunden
- User      	Die User können entweder intern sein oder ein externer Kunde.


Entwurfsentscheidungen
======================

**Inhalt.**

Für die Kommunikation der API und der Datenbank wurde das Entityframework verwendet.
Bis jetzt hatte die Entwicklung damit keine Erfahrung und es kann sein, dass dies eine Fehlentscheidung war.
Der Grund für das benutzen des Entityframeworks war, dass es viele vereinfachungen bieten soll und die Datenbank durch den Code erstellt werden kann.

Qualitätsanforderungen
======================

**Inhalt.**

Folgend dieunstrukturierten Anforderungen an das System:
 - Der Sachbearbeiter kann Kunden mit Namen und Adresse und Kundennummer im System verwalten,
d.h. erfassen, bearbeiten, löschen und den Kunden mit dessen Namen oder Kundennummer suchen.
 - Der Sachbearbeiter kann zudem die Autos von CarRent verwalten und nach denen suchen.
  - Jedes Auto kann einer bestimmten Klasse zwischen Luxusklasse, Mittelklasse oder Einfachklasse
zugeordnet werden und besitzt zudem eine Marke, einen Typ und eine eindeutige Identifikation.
 - Jede Klasse besitzt eine Tagesgebühr.
 - Bei einer neuen Reservation kann der Kunde ein Auto aus einer bestimmten Klasse wählen. Er muss
zudem die Anzahl der Tage angeben, die er das Auto gerne mieten möchte. Dabei werden die
Gesamtkosten berechnet. Wird die Reservation gespeichert, so wird sie mit einer Reservationsnummer
ablegt.
 - Bei Abholung des Autos wird die Reservation in einen Mietvertrag umgewandelt.